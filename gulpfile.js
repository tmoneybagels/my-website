var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

var autoprefixOptions = {
	browsers: ['last 2 versions', '> 5%']
};

gulp.task('styles', async function(){
	gulp.src('sass/**/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer(autoprefixOptions))
	.pipe(gulp.dest('./public/stylesheets/'));
});

gulp.task('default', function(){
	gulp.watch('sass/**/*.scss',gulp.series('styles'));
});