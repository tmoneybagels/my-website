window.onload = () => {
  // waypoints
  function createWaypoint(elementName, className) {
    let target = document.getElementById(elementName);
    let waypoint = new Waypoint({
      element: target,
      handler: function() {
        target.classList.remove('hidden');
        target.classList.add(className);
      },
      offset: '75%'
    })

    return waypoint;
  }

  const aboutWaypoint = createWaypoint('about', 'fadedown--2');
  const skillstWaypoint = createWaypoint('skills--head', 'fadedown--2');
  const skills1Waypoint = createWaypoint('skills--1', 'fadedown--1');
  const skills2Waypoint = createWaypoint('skills--2', 'fadedown--2');
  const skills3Waypoint = createWaypoint('skills--3', 'fadedown--3');
  const skills4Waypoint = createWaypoint('skills--4', 'fadedown--4');
  const projectsWaypoint = createWaypoint('projects--head', 'fadedown--2');
  const projects1Waypoint = createWaypoint('projects--ypf', 'fadedown--1');
  const projects2Waypoint = createWaypoint('projects--skuCreator', 'fadedown--2');
  const projects3Waypoint = createWaypoint('projects--yelpCamp', 'fadedown--1');
  const projects4Waypoint = createWaypoint('projects--github', 'fadedown--2');
  const contactWaypoint = createWaypoint('contact', 'fadedown--2');
}